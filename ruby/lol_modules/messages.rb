module Messages

  def make_msg(msg_type, data)
    JSON.generate({:msgType => msg_type, :data => data})
  end
  
end