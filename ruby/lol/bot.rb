require 'date'
require 'csv'

class Bot
  include Messages 

  attr_accessor :track, :cars, :my_car, :my_car_color
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msg_type = message['msgType']
      msg_data = message['data']
      case msg_type
        when 'carPositions'
          game_tick = message["gameTick"]
          update_positions msg_data, game_tick
          tcp.puts my_car.next_message game_tick
        else
          case msg_type
            when 'yourCar'
              init_my_car msg_data
            when 'gameInit'
              init_race msg_data
            when 'join'
              # puts "Joined #{msg_data}"
            when 'gameStart'
              ap 'Race started'
            when 'crash'
              ap 'Someone crashed'
            when 'gameEnd'
              ap 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
            when 'lapFinished'
              ap "Lap finished: #{msg_data}"
            when 'tournamentEnd'
              CSV.open("../results/positions#{DateTime.now.strftime('%Y%m%d')}.csv", "w") do |csv|
                my_car.commands.each do |command|
                  csv << [command[:game_tick], command[:message]]
                end
              end
            else 
              ap msg_type
          end
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("joinRace", {botId: {:name => bot_name, :key => bot_key}, trackName: "germany", carCount: 1})
  end

  def ping_message
    make_msg("ping", {})
  end

  def init_my_car(car_data)
    @my_car_color = car_data["color"]
  end

  def get_car_by_color color
    cars.find{|c| c.color == color}
  end

  def init_race(race_data)
    @track = Track.from_msg(race_data["race"]["track"])
    @cars = Car.load_all(race_data["race"]["cars"], track)
    @my_car = get_car_by_color my_car_color
  end

  def update_positions(positions, game_tick)
    positions.each do |pos|
      car = get_car_by_color pos["id"]["color"]
      car.update_position pos, game_tick
    end
  end
end