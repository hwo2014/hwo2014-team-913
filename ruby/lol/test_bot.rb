class TestBot

  def initialize()
    play
  end

private

  def play()
    react_to_messages_from_server
  end

  def react_to_messages_from_server()
    if json = get_sample_data
      message = JSON.parse(json)
      msg_type = message['msgType']
      msg_data = message['data']
      case msg_type
        when 'carPositions'
          # ap msg_data
          update_positions msg_data
        else
          case msg_type
          when 'yourCar'
              init_my_car msg_data
            when 'gameInit'
              init_race msg_data
            # when 'join'
            #   puts 'Joined'
            when 'gameStart'
              ap 'Race started'
            # when 'crash'
            #   puts 'Someone crashed'
            when 'gameEnd'
              ap 'Race ended'
            # when 'error'
            #   puts "ERROR: #{msgData}"
            else 
              ap msg_type
          end
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msg_type, data)
    JSON.generate({:msgType => msg_type, :data => data})
  end

  def init_race(race_data)
    race = Race.from_msg race_data
  end

  def get_sample_data
    File.open("../sample_data/game_init_response.json").read
  end
end