class CarPosition
  attr_accessor :angle, :piece_index, :in_piece_distance, :start_lane, :end_lane, :lap

  def initialize angle, piece_index, in_piece_distance, start_lane, end_lane, lap
    @angle, @piece_index, @in_piece_distance, @start_lane, @end_lane, @lap = angle, piece_index, in_piece_distance, start_lane, end_lane, lap
  end

  def self.from_msg pos
    new pos["angle"], pos["piecePosition"]["pieceIndex"], pos["piecePosition"]["inPieceDistance"], pos["piecePosition"]["lane"]["startLaneIndex"], pos["piecePosition"]["lane"]["endLaneIndex"], pos["piecePosition"]["lap"]
  end
end