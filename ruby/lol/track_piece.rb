class TrackPiece
  def self.from_msg msg
    if msg["radius"]
      CurvedTrackPiece.from_msg msg
    else
      StraightTrackPiece.from_msg msg
    end
  end
end

class StraightTrackPiece < TrackPiece
  attr_accessor :length, :switch

  def initialize length, switch
    @length, @switch = length, switch
  end

  def length lane
    @length
  end

  def to_s
    "Straight (#{length}, #{switch})"
  end

  def self.from_msg msg
    new msg["length"], (msg["switch"] || false)
  end
end

class CurvedTrackPiece < TrackPiece
  attr_accessor :angle, :radius, :switch

  def initialize angle, radius, switch
    @angle, @radius, @switch = angle, radius, switch
  end

  def length lane
    angle.abs * (2*Math::PI * radius_for_lane(lane))/360.0
  end

  def to_s
    "#{self.class_name} (#{angle}, #{radius}) length = #{length}"
  end

  def self.from_msg msg
    angle = msg["angle"].to_f
    klass = angle < 0 ? LeftCurvedTrackPiece : RightCurvedTrackPiece
    klass.new angle.abs, msg["radius"], (msg["switch"] || false)
  end
end

class LeftCurvedTrackPiece < CurvedTrackPiece
  def radius_for_lane lane
    radius + lane.distance_from_center
  end
end

class RightCurvedTrackPiece < CurvedTrackPiece
  def radius_for_lane lane
    radius - lane.distance_from_center
  end
end