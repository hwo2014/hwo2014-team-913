class Lane

  attr_accessor :distance_from_center, :index

  def initialize distance_from_center, index
    @distance_from_center, @index = distance_from_center, index
  end

  def to_s
    "Lane (#{distance_from_center}, #{index})"
  end

  def self.from_msg msg
    new msg["distanceFromCenter"], msg["index"]
  end
end