class Race

  attr_accessor :track, :cars, :race_session

  def initialize track, cars, race_session
    @track, @cars, @race_session = track, cars, race_session
  end

  def to_s 
    "Race:\n#{track}" 
  end

  def self.from_msg msg
    race_data = msg["race"]
    race = new Track.from_msg(race_data["track"]), nil, nil
    puts race.track.preferred_current_lane(3)
    race
  end
  
end