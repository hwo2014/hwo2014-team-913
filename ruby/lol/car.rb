class Car
  include Messages 

  attr_accessor :name, :color, :length, :width, :guide_flag_position, :tick_data, :commands, :current_pos, :last_pos, :track

  def initialize name, color, length, width, guide_flag_position, track
    @name, @color, @length, @width, @guide_flag_position, @track = name, color, length, width, guide_flag_position, track
    @tick_data = []
    @commands = []
  end

  def update_position pos, game_tick=0
    game_tick ||= 0
    @last_pos = current_pos
    @current_pos = @tick_data[game_tick] = CarPosition.from_msg pos
  end

  def switch_direction
    preferred = track.preferred_current_lane(current_pos.piece_index+1).index

    if preferred
      switch = preferred - current_pos.end_lane
      if switch < 0
        "Left"
      elsif switch > 0
        "Right"
      end
    end
  end

  def next_message game_tick
    if next_piece_switch? && (switch_dir = switch_direction)
      message = switch_lane_message switch_dir
    else
      message = throttle_message 0.6
    end
    commands << {game_tick: game_tick, message: message}
    message
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_lane_message(direction)
    make_msg("switchLane", direction)
  end
  
  def new_piece?
    !last_pos || current_pos.piece_index != last_pos.piece_index
  end

  def next_piece_switch?
    new_piece? && track.next_piece(current_pos.piece_index).switch
  end

  def self.load_all car_array, track
    car_array.map{|car| Car.from_msg car, track }
  end

  def self.from_msg msg, track
    new msg["id"]["name"], msg["id"]["color"], msg["dimensions"]["length"], msg["dimensions"]["width"], msg["dimensions"]["guideFlagPosition"], track
  end
end