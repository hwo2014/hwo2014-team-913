class Track

  attr_accessor :id, :name, :pieces, :lanes

  def initialize id, name, pieces, lanes
    @id, @name, @pieces, @lanes = id, name, pieces, lanes
  end

  def next_switch piece_index
    next_switch = pieces[piece_index+1..-1].find_index{|piece| piece.switch } 
    if next_switch
      next_switch += piece_index
    else
      next_switch = pieces[0..piece_index].find_index{|piece| piece.switch } 
    end
  end

  def distance_to_next_switch piece_index, lane
    pieces_between(piece_index, next_switch(piece_index)).inject(0) {|dist, p| dist+p.length(lane)}
  end

  def pieces_between start_index, end_index
    if end_index > start_index
      pieces[start_index...end_index]
    else
      pieces[start_index..-1] + pieces[0...end_index]
    end
  end

  def next_piece index
    pieces[(index+1)%pieces.length]
  end

  def preferred_current_lane piece_index
    distances = lanes.map{|lane| distance_to_next_switch piece_index, lane}
    min = 0
    all_same = true
    distances.each_with_index do |distance, i|
      min = i if distance < distances[min]
      all_same = all_same && distance != distances[min]
    end

    if !all_same
      lanes[min]
    else
      nil
    end
  end

  def to_s
    "Track (id: #{id}, name: #{name})\nPieces:\n#{pieces.map{|p| "\t#{p}" }.join("\n")}\nLanes:\n#{lanes.map{|p| "\t#{p}" }.join("\n")}\n"
  end

  def self.from_msg msg
    new msg["id"], msg["name"], read_pieces(msg["pieces"]), read_lanes(msg["lanes"])
  end

  def self.read_pieces pieces
    pieces.map{ |p| TrackPiece.from_msg p }
  end

  def self.read_lanes lanes
    lanes.map{ |l| Lane.from_msg l }
  end

end