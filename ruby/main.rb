require 'json'
require 'socket'
require 'awesome_print'
Dir[File.join(".","lol_modules/**/*.rb")].each {|f| require f }
Dir[File.join(".","lol/**/*.rb")].each {|f| require f }

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port} with key: #{bot_key}"

Bot.new(server_host, server_port, bot_name, bot_key)
# TestBot.new